---
# version of caddy to use
caddy_version: "latest"
# consul version to use
caddy_consul_consul_version: "latest"

# please define manually an acl token for consul (uuid-4)
# > irb
#  require 'securerandom'
#
#  uuid = SecureRandom.uuid
consul_acl_master_token: ""

# name of the caddy network
caddy_network: "caddy-public"

# caddy public-tag, this tag is used to mark a service as public
caddy_public_tag: "caddy-public"

# for backwards compatibility (and mailserver), we stick with RSA, waiting LE supporting ed25519, P- curves are not trusted (NSA)
caddy_acme_keytype: "rsa4096"

# let's encrypt email for certificates
lets_encrypt_email: ""

# domain used by consul UI
consul_ui_domain: "consul.{{ hostname }}"


# host port used to expose caddy metrics
caddy_metrics_port: 9500
# list of ips allowed to retrieve caddy metrics
caddy_metrics_allowed_ips: []

# number of consul replicas (try to align with number of caddy replicas for HA)
consul_replicas: "{{ ( groups['swarm_managers'] | length ) + 1 }}"

# docker restart policies (0 for unlimited)
caddy_max_attempts: 0
consul_max_attempts: 0

# ports used by caddy
caddy_http_port: 80
caddy_https_port: 443

# decides which typology to run Caddy, see 
caddy_use_standalone_mode: True

# for tests, use CA https://acme-staging-v02.api.letsencrypt.org/directory, do not define for production
caddy_custom_acme_ca: ""

# Caddy Server Options (global) https://caddyserver.com/docs/caddyfile/options#server-options
caddy_servers_directives: |
  servers :443 {
    metrics
  }

caddy_security_portal_url: portal.domain.tld
caddy_security_enabled: false
caddy_security_client_id: test-caddy
caddy_security_client_secret: ""
caddy_security_metadata_url: https://auth.domain.tld/realms/master/.well-known/openid-configuration
caddy_security_crypto_key: ""


# enable debug?
caddy_debug: false

# Causes all certificates to be issued internally by default
caddy_local_certs: false

# configure sysctl to accept a given number of connections
caddy_max_connections_sysctl: 4096

# Caddyfile additional content
caddy_caddyfile_content: ""
# caddy_caddyfile_content: |


# key used to encrypt tls certificates stored in consul, change it needed
caddy_consul_certificates_aes_key: "consultls-1234567890-caddytls-32"

# The encryption of the overlay networks has a bug that is  causing issues for one service to reach another over the overlay
# network. See moby/moby#37115 - https://github.com/moby/moby/issues/37115
# On some cloud provider, you might want to disable this encryption to get a working service
caddy_internal_network_encryption_enabled: True

# minimal TLS version
# global TLS configuration is not supported (https://github.com/caddyserver/caddy/issues/3591), configure per site needs
# UNSUPPORTED: caddy_min_tls_version: "VersionTLS12"

# list of supported ciphers; the server will fail if one cipher is not valid
# UNSUPPORTED: caddy_cipher_suites:
#    # TLS 1.3
#    - TLS_AES_128_GCM_SHA256
#    - TLS_AES_256_GCM_SHA384
#    - TLS_CHACHA20_POLY1305_SHA256
#    # < 1.3
#    - TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384
#    - TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384
#    - TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305
#    - TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305
#    - TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256
#    - TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256
...