---
version: "3.8"

services:

  docker-registry-proxy:
    image: "ghcr.io/lolhens/docker-registry-cache:{{ docker_registry_proxy_version }}"
    environment:
      CONFIG: |
        {{ docker_registry_proxy_config|to_json }}
    volumes:
      - registry-cache:/var/lib/registry
    networks:
      - {{ caddy_network }}
    deploy:
      placement:
        constraints:
          - node.role == manager
      restart_policy:
        condition: any
        delay: 5s
        window: 45s
      labels:
        caddy: "{{ docker_registry_proxy_domain }}"
        # enable access log
        caddy.log: ""

        # hsts headers (see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Strict-Transport-Security)
        caddy.1_header.Strict-Transport-Security: "\"max-age=63072000; includeSubDomains; preload\""

        # security labels
        caddy.1_header.-Server: ""
        caddy.1_header.-X-Powered-By: ""
        caddy.1_header.Permissions-Policy: "\"accelerometer=(), camera=(), interest-cohort=(), geolocation=(), gyroscope=(), magnetometer=(), microphone=(), payment=(), usb=()\"" # see best practices https://github.com/w3c/webappsec-permissions-policy/blob/master/features.md, https://developer.mozilla.org/en-US/docs/Web/HTTP/Feature_Policy
        caddy.1_header.Referrer-Policy: "same-origin"
        caddy.1_header.X-Content-Type-Options: "nosniff"
        caddy.1_header.X-Frame-Options: "DENY"
        caddy.1_header.X-XSS-Protection: "\"1; mode=block\""
        caddy.1_header.Content-Security-Policy: "\"default-src 'self'; script-src 'self'; img-src 'self'; style-src 'self' 'unsafe-inline'; font-src 'self' data:; frame-ancestors 'self'; frame-src 'self'; object-src 'none';\"" # https://csp-evaluator.withgoogle.com/ content security policy

        # ip filter auth & reverse proxy conf
        caddy.2_@ipfilter.remote_ip: "{{ docker_registry_proxy_allowed_ips|join(' ') }} 127.0.0.1"

        # apply ip filter
        caddy.3_handle: "@ipfilter"

        # try to match filter for a reverse proxy to upstream (eventually add load balancing etc)
        caddy.3_handle.2_reverse_proxy: "{% raw %}{{upstreams 5000}}{% endraw %}"
        caddy.3_handle.2_reverse_proxy.lb_policy: "round_robin"

        # or reject
        caddy.4_respond: "`Origin not allowed` 403"

  # auto cleanup with cron
  cache-registry-cleaner:
    image: "anoxis/registry-cli:latest"
    command: "-r https://{{ docker_registry_proxy_domain }} --delete --num {{ docker_registry_proxy_cache_num_to_keep }} --keep-by-hours {{ docker_registry_proxy_cache_keep_max_age }} --keep-tags-like {{ docker_registry_proxy_cache_keep_tags|map('quote')|join(' ') }}"
    deploy:
      labels:
        - "swarm.cronjob.enable=true"
        - "swarm.cronjob.schedule={{ docker_registry_proxy_cache_cleaner_schedule }}" 
        - "swarm.cronjob.skip-running=true" #Do not start a job if the service is currently running.
      replicas: 1
      restart_policy:
        condition: on-failure
      placement:
        preferences:
          - spread: node.id
        max_replicas_per_node: 1

networks:
  {{ caddy_network }}:
    external: true

volumes:
  registry-cache:
    driver: local
...