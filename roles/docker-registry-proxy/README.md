# docker-registry-proxy

This role setups a docker registry proxy which enables caching capabilities of the pulled images, avoid pull limits and ensure presence of a local copy of the images used in the cluster.

## Details 

See : https://github.com/LolHens/docker-registry-cache

## Configuration

Parameters to configure are listed in `defaults/main.yml`, you should define a custom `docker_registry_proxy_domain` and adjust `docker_registry_proxy_cache_max_size` to your needs.

List the proxied registries using `docker_registry_proxy_config`.