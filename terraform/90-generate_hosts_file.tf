# generate the output files of the infrastructure

resource "local_file" "hosts_file" {
  filename = "${path.module}/generated/hosts"
  content  = templatefile("${path.module}/templates/hosts.tpl", {
    instances         = module.deepcube_swarm_cluster[*].instance
  })

  directory_permission  = 0700
  file_permission       = 0600
}