#!/bin/bash

# To use an Openstack cloud you need to authenticate against keystone, which
# returns a **Token** and **Service Catalog**. The catalog contains the
# endpoint for all services the user/tenant has access to - including nova,
# glance, keystone, swift.
#
export OS_AUTH_URL=https://auth.cloud.ovh.net/v3/
export OS_IDENTITY_API_VERSION=3

export OS_USER_DOMAIN_NAME=${OS_USER_DOMAIN_NAME:-"Default"}
export OS_PROJECT_DOMAIN_NAME=${OS_PROJECT_DOMAIN_NAME:-"Default"}


# With the addition of Keystone we have standardized on the term **tenant**
# as the entity that owns the resources.
export OS_TENANT_ID=
export OS_TENANT_NAME=""

# In addition to the owning entity (tenant), openstack stores the entity
# performing the action as the **user**.
export OS_USERNAME="user-"

# With Keystone you pass the keystone password.
echo "Please enter your OpenStack Password: "
read -sr OS_PASSWORD_INPUT
export OS_PASSWORD=$OS_PASSWORD_INPUT

# If your configuration has multiple regions, we set that information here.
# OS_REGION_NAME is optional and only valid in certain environments.
export OS_REGION_NAME="GRA"

# Don't leave a blank variable, unset it if it was empty
if [ -z "$OS_REGION_NAME" ]; then
  unset OS_REGION_NAME;
fi

# Terraform state backend configuration
export OS_TF_STATE_SWIFT_CONTAINER="tfstate-deepcube-wp2-swarm"
export OS_TF_STATE_SWIFT_ARCHIVE_CONTAINER="tfstate-deepcube-wp2-swarm-archive"
export OS_TF_STATE_SWIFT_REGION_NAME="GRA"

# generate the version terraform file
SCRIPT=`realpath $0`
SCRIPTPATH=`dirname $SCRIPT`

jinja2 "$SCRIPTPATH/01-versions.tf.j2" > "$SCRIPTPATH/01-versions.tf"
jinja2 "$SCRIPTPATH/00-providers.tf.j2" > "$SCRIPTPATH/00-providers.tf"

# export to TF / OVH

export TF_VAR_OS_AUTH_URL="$OS_AUTH_URL"
export TF_VAR_OS_USER_DOMAIN_NAME="$OS_USER_DOMAIN_NAME"
export TF_VAR_OS_PROJECT_DOMAIN_NAME="$OS_PROJECT_DOMAIN_NAME"
export TF_VAR_OS_TENANT_ID="$OS_TENANT_ID"
export TF_VAR_OS_TENANT_NAME="$OS_TENANT_NAME"
export TF_VAR_OS_USERNAME="$OS_USERNAME"
export TF_VAR_OS_PASSWORD="$OS_PASSWORD"
export TF_VAR_OS_REGION_NAME "$OS_REGION_NAME"