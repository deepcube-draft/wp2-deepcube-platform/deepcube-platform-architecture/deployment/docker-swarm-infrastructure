# Setup the required networks for the Swarm

# Beginning of the Swarm internal network
resource "openstack_networking_network_v2" "os_network_deepcube_swarm_inet" {
  for_each       = local.OS_CLUSTER_REGIONS
  
  name           = var.CLUSTER_SWARM_PRIVATE_NETWORK_NAME
  admin_state_up = "true"

  region = each.key
}

# Private subnet
resource "openstack_networking_subnet_v2" "os_subnetwork_deepcube_swarm_inet" {
  for_each        = local.OS_CLUSTER_REGIONS

  name            = "sub${var.CLUSTER_SWARM_PRIVATE_NETWORK_NAME}"
  network_id      = openstack_networking_network_v2.os_network_deepcube_swarm_inet[each.key].id
  cidr            = var.CLUSTER_SWARM_PRIVATE_NETWORK_CIDR
  enable_dhcp     = true
  no_gateway      = true
  ip_version      = 4
  dns_nameservers = var.BASE_DEFAULT_DNS_SERVERS

  region = each.key 
}
# End of the Swarm internal network