#!/usr/bin/env bash

# due to lack of OVH api mapping in terraform, we must retrieve the vrack id of the project
# using the raw api

OVH_APP_SECRET="$OVH_APPLICATION_SECRET"
OVH_CONSUMER_KEY="$OVH_CONSUMER_KEY"
OVH_APP_KEY="$OVH_APPLICATION_KEY"

HTTP_METHOD="GET"
HTTP_QUERY="https://api.ovh.com/1.0/cloud/project/${OS_TENANT_ID}/vrack"
HTTP_BODY=""

TIME=$(curl -s https://api.ovh.com/1.0/auth/time)

CLEAR_SIGN="$OVH_APP_SECRET+$OVH_CONSUMER_KEY+$HTTP_METHOD+$HTTP_QUERY+$HTTP_BODY+$TIME"
SIG='$1$'$(echo -n $CLEAR_SIGN | openssl dgst -sha1 | sed -e 's/^.* //')


response=$(curl -s -X $HTTP_METHOD \
$HTTP_QUERY \
-H "Content-Type: application/json" \
-H "X-Ovh-Application: $OVH_APP_KEY" \
-H "X-Ovh-Timestamp: $TIME" \
-H "X-Ovh-Signature: $SIG" \
-H "X-Ovh-Consumer: $OVH_CONSUMER_KEY" | jq '.id' | tr -d '\"')

echo "$response"