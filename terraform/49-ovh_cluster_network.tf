
# Configure OVH Network using https://github.com/ovh/terraform-ovh-commons/blob/master/examples/multiregion-pci/modules/infra/main.tf

resource "openstack_networking_network_v2" "ovh_network_deepcube_swarm_inet" {
  for_each     = local.OVH_CLUSTER_REGIONS

  admin_state_up = "true"
  name           = var.CLUSTER_SWARM_PRIVATE_NETWORK_NAME

  value_specs = {
    "provider:network_type"    = "vrack"
    "provider:segmentation_id" = var.OVH_NETWORKS_VLAN_ID
  }

  region = each.key
}


# Private subnet
resource "openstack_networking_subnet_v2" "ovh_subnetwork_deepcube_swarm_inet" {
  for_each        = local.OVH_CLUSTER_REGIONS

  cidr            = var.CLUSTER_SWARM_PRIVATE_NETWORK_CIDR
  dns_nameservers = var.BASE_DEFAULT_DNS_SERVERS
  enable_dhcp     = true
  ip_version      = 4

  name            = "sub${var.CLUSTER_SWARM_PRIVATE_NETWORK_NAME}"
  network_id      = openstack_networking_network_v2.ovh_network_deepcube_swarm_inet[each.key].id

  no_gateway      = true

  allocation_pool {
    start = cidrhost(var.CLUSTER_SWARM_PRIVATE_NETWORK_CIDR, 10)
    end   = cidrhost(var.CLUSTER_SWARM_PRIVATE_NETWORK_CIDR, -10)
  }

  region = each.key 
}